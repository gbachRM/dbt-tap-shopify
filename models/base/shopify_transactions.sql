with source as (

    select * from {{var('schema')}}.transactions

),

renamed as (

    select

        id as transaction_id,
        order_id,
        parent_id,
        
        created_at::timestamp as created_at,

        kind,
        status,

        currency,
        amount

    from source

)

select * from renamed
