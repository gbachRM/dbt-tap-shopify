with source as (

    select * from {{var('schema')}}.products

),

renamed as (

    select

        id as product_id,
        
        created_at,
        published_at,
        updated_at,

        title

    from source

)

select * from renamed
