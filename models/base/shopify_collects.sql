with source as (

    select * from {{var('schema')}}.collects

),

renamed as (

    select

        id as collect_id,
        collection_id,
        product_id,

        created_at,
        updated_at

    from source

)

select * from renamed
