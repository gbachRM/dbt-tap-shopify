with source as (

    select * from {{var('schema')}}.orders

),

renamed as (

    select

        (line_items.id)::bigint as order_line_item_id,
        _sdc_source_key_id as order_id,
        (line_items.product_id)::bigint as product_id,
        (line_items.variant_id)::bigint as variant_id,

        (line_items.quantity)::int as quantity,
        currency,

        (line_items.price)::numeric as price,
        (line_items.total_discount)::numeric as total_discount

    from source
        LEFT JOIN {{var('schema')}}.orders__line_items as line_items ON line_items._sdc_source_key_id = id

--         source,
--         jsonb_array_elements(source.orders__line_items) line_items(child)
)

select * from renamed
