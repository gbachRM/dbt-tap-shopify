with source as (

    select * from {{var('schema')}}.order_refunds

),

orders as (

    select * from {{var('schema')}}.orders

),

renamed as (

    select

        (line_items.child->'id')::bigint as refund_line_item_id,
        id as refund_id,
        order_id as order_id,
        (line_items.child->'line_item_id')::bigint as order_line_item_id,

        (line_items.child->'quantity')::int as quantity,
        (line_items.child->'subtotal')::numeric as subtotal,
        (line_items.child->'total_tax')::numeric as total_tax

    from

        source,
        jsonb_array_elements(source.refund_line_items) line_items(child)

)

SELECT

    renamed.*,

    orders.currency as currency

FROM renamed
JOIN orders
ON orders.id = renamed.order_id
