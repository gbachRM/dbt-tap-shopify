with source as (

    select * from {{var('schema')}}.orders__refunds

),

renamed as (

    select

        id as refund_id,
        _sdc_source_key_id,

        created_at,
        processed_at::timestamp as processed_at

    from source

)

select * from renamed
