with source as (

    select * from {{var('schema')}}.abandoned_checkouts

),

renamed as (

    select

        id as checkout_id,
        customer__id as customer_id,

        closed_at,
        completed_at,
        created_at,
        updated_at,

        source_name,
        case
            when source_name = 'web' or source_name = '580111'
                then 'Online Store'
            when source_name = 'shopify_draft_order'
                then 'Draft Orders'
            when source_name = 'pos'
                then 'Point of Sale'
            when source_name = 'iphone'
                then 'Shopify POS for iPhone'
            when source_name = 'android'
                then 'Shopify POS for Android'
            else source_name
        end as source_label,
        presentment_currency,

        subtotal_price,
        COALESCE(total_discounts, 0.0) as total_discounts,
        total_line_items_price,
        total_price,
        COALESCE(total_tax, 0.0) as total_tax

    from source

)

select * from renamed
