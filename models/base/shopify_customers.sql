with source as (

    select * from {{var('schema')}}.customers

),

renamed as (

    select

        id as customer_id,

        created_at,
        updated_at,

        first_name,
        last_name

    from source

)

select * from renamed
