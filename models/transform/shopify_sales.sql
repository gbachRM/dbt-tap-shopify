WITH orders AS (

    SELECT *
    FROM {{ref('shopify_orders')}}

),

order_shipping_lines AS (

    SELECT *
    FROM {{ref('shopify_order_shipping_lines')}}

),

order_refunds AS (

    SELECT *
    FROM {{ref('shopify_order_refunds')}}

),

refund_line_items AS (

    SELECT *
    FROM {{ref('shopify_refund_line_items')}}

),

refund_order_adjustments AS (

    SELECT *
    FROM {{ref('shopify_refund_order_adjustments')}}

),

sales AS (

    SELECT

        order_id,
        processed_at,

        'order'   as sale_type,
        'product' as sale_line_type,
        1         as order_count,

        currency,

        total_line_items_price                                as gross_sales,
        0 - total_discounts                                   as discounts,
        0                                                     as returns,
        total_line_items_price - total_discounts              as net_sales, -- gross_sales + discounts
        0                                                     as shipping,
        total_tax                                             as taxes,
        total_line_items_price - total_discounts + total_tax  as total_sales -- net_sales + taxes

    FROM orders

    UNION

    SELECT

        orders.order_id,
        orders.processed_at,

        'order'     as sale_type,
        'shipping'  as sale_line_type,
        0           as order_count,

        order_shipping_lines.currency as currency,

        0     as gross_sales,
        0     as discounts, -- already included in order/product row
        0     as returns,
        0     as net_sales,
        price as shipping,
        0     as taxes, -- already included in order/product row
        price as total_sales -- shipping

    FROM order_shipping_lines
    JOIN orders
    ON orders.order_id = order_shipping_lines.order_id

    UNION

    SELECT

        order_refunds.order_id,
        order_refunds.processed_at,

        'return'  as sale_type,
        'product' as sale_line_type,
        0         as order_count,

        refund_line_items.currency as currency,

        0                         as gross_sales,
        0                         as discounts,
        0 - subtotal              as returns,
        0 - subtotal              as net_sales, -- returns
        0                         as shipping,
        0 - total_tax             as taxes,
        0 - subtotal - total_tax  as total_sales -- net_sales + taxes

    FROM refund_line_items
    JOIN order_refunds
    ON order_refunds.refund_id = refund_line_items.refund_id

    UNION

    SELECT

        order_refunds.order_id,
        order_refunds.processed_at,

        'return'    as sale_type,
        'shipping'  as sale_line_type,
        0           as order_count,

        refund_order_adjustments.currency as currency,

        0                   as gross_sales,
        0                   as discounts,
        0                   as returns,
        0                   as net_sales,
        amount              as shipping,
        tax_amount          as taxes,
        amount + tax_amount as total_sales -- shipping + taxes

    FROM refund_order_adjustments
    JOIN order_refunds
    ON order_refunds.refund_id = refund_order_adjustments.refund_id
    WHERE refund_order_adjustments.kind = 'shipping_refund'

    UNION

    SELECT

        order_refunds.order_id,
        order_refunds.processed_at,

        'return'  as sale_type,
        'unknown' as sale_line_type,
        0         as order_count,

        refund_order_adjustments.currency as currency,

        0                   as gross_sales,
        0                   as discounts,
        amount              as returns,
        amount              as net_sales, -- returns
        0                   as shipping,
        tax_amount          as taxes,
        amount + tax_amount as total_sales -- net_sales + taxes

    FROM refund_order_adjustments
    JOIN order_refunds
    ON order_refunds.refund_id = refund_order_adjustments.refund_id
    WHERE refund_order_adjustments.kind = 'refund_discrepancy'
)

SELECT

    *,

    EXTRACT(DAY FROM processed_at) processed_day,
    EXTRACT(WEEK FROM processed_at) processed_week,
    EXTRACT(MONTH FROM processed_at) processed_month,
    EXTRACT(YEAR FROM processed_at) processed_year,
    EXTRACT(ISOYEAR FROM processed_at) processed_iso_year

FROM

    sales