with sales as (

     select *
     from {{ref('shopify_sales')}}

),

dates as (

     select *
     from {{ref('shopify_dates')}}

),

aggregated as (

  select

    date_trunc('week', MIN(processed_at))::date as week_start,

    currency,

    SUM(order_count) as order_count,
    SUM(gross_sales) as gross_sales,
    SUM(discounts) as discounts,
    SUM(returns) as returns,
    SUM(net_sales) as net_sales,
    SUM(shipping) as shipping,
    SUM(taxes) as taxes,
    SUM(total_sales) as total_sales

  from sales

  group by
    processed_iso_year,
    processed_week,
    currency

  order by
    processed_iso_year,
    processed_week,
    currency

)

SELECT 

  dates.date_actual as week_start,

  -- Generate a descriptive label: "[2019-12-09,2019-12-15]"
  CONCAT('[', dates.first_day_of_week, ',', dates.last_day_of_week, ']') as label,

  currency,

  COALESCE(order_count, 0) as order_count,
  COALESCE(gross_sales, 0) as gross_sales,
  COALESCE(discounts, 0) as discounts,
  COALESCE(returns, 0) as returns,
  COALESCE(net_sales, 0) as net_sales,
  COALESCE(shipping, 0) as shipping,
  COALESCE(taxes, 0) as taxes,
  COALESCE(total_sales, 0) as total_sales

FROM dates
LEFT OUTER JOIN aggregated
ON aggregated.week_start = dates.date_actual 
WHERE dates.day_of_week = 1